# AssuerPlus

AssuerPlus est en entreprise spécialisée dans l’assurance auto depuis 20 ans. L’entreprise est en croissance continue et a autour de 20K clients partout en France.

L’objective du projet est de fournir les assurés une application web et autre mobile (une seule application responsive est acceptée comme solution) simple et facile à utiliser. 

En cas d’accident, l’assuré se connecte directement par son téléphone, charge les photos de l’accident et lance une déclaration de sinistre. En même temps, l’application met l’assuré en contacte directe est rapide avec un service de remorquage automobile le plus proche et un garage de réparation partenaire.

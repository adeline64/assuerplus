# Assuer Plus

## API Reference

# Address
**Get all addresses**
----
Returns array json data.

* **URL**

      api/addresses


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "town": "PAU",
                          "postalCode": "64000",
                          "address": "6 chemin de la paix"
                      },
                      {
                          "id": 2,
                          "town": "PAU",
                          "postalCode": "64000",
                          "address": "34 rue Gaston Lacoste"
                      },
                      {
                          "id": 3,
                          "town": "PAU",
                          "postalCode": "64000",
                          "address": "3 rue du saint"
                      }
                  ]
          ```


* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/addresses",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```
---


**Get address by id**
----
Returns json data about a single address.

* **URL**

      api/addresses/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200
          **Content:**
          ```json
                  {
                      "id": 1,
                      "town": "PAU",
                      "postalCode": "64000",
                      "address": "6 chemin de la paix"
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`



* **Sample Call:**

  ```javascript
    fetch({
      url: "api/addresses/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Add address**
----
Save a single address in the data base.

* **URL**

      api/addresses

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

  ```json
        {
           "town": "PAU",
           "postalCode": "64000",
           "address": "6 chemin de la paix"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
                {
                    "id": 1,
                    "town": "PAU",
                    "postalCode": "64000",
                    "address": "6 chemin de la paix"
                }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/addresses",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Update address**
----
Update json data about a single address.

* **URL**

      api/addresses/:id

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  ```json
        {
           "town": "PAU",
           "postalCode": "64000",
           "address": "9 chemin de la paix"
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
            {
                "id": 1,
                "town": "PAU",
                "postalCode": "64000",
                "address": "9 chemin de la paix"
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/addresses/1",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Delete address**
----
Delete json data about a single address.

* **URL**

      api/addresses/:id

* **Method:**

  `DELETE`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/addresses/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```

---

# User
**Get all user**
----
Returns array json data.

* **URL**

      api/users


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK
              **Content:**
          ```json
                  [
                      {
                          "id": 1,
                          "firstName": "melanie",
                          "lastName": "Bordenave",
                          "email": "melanie.bordenave@outlook.com",
                          "password": "453Out*",
                          "picture": "image1",
                          "address": {
                                      "id": 1,
                                      "town": "PAU",
                                      "postalCode": "64000",
                                      "address": "9 chemin de la paix"}
                          },
                          {
                              "id": 2,
                              "firstName": "hermione",
                              "lastName": "Bordenave",
                              "email": "hermione.bordenave@outlook.com",
                              "password": "Rejhgf34*",
                              "picture": "image1",
                              "address": {
                                      "id": 2,
                                      "town": "PAU",
                                      "postalCode": "64000",
                                      "address": "34 rue Gaston Lacoste"
                              }
                          }
                  ]
            ```

* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/users",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```
---


**Get user by id**
----
Returns json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200
          **Content:**
          ```json
                  {
                    "id": 2,
                    "firstName": "hermione",
                    "lastName": "Bordenave",
                    "email": "hermione.bordenave@outlook.com",
                    "password": "Rejhgf34*",
                    "picture": "image1",
                    "address": {
                      "id": 2,
                      "town": "PAU",
                      "postalCode": "64000",
                      "address": "34 rue Gaston Lacoste"
                    }
                  }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`



* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Add user**
----
Save a single user in the data base.

* **URL**

      api/users

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

  ```json
        {
          "firstName": "Julie",
          "lastName": "Fayet",
          "email": "julie.fayet@outlook.com",
          "password": "3465Tyue**",
          "picture": "image1",
          "address": {
            "id": 1
          }
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
                {
                    "id": 3,
                    "firstName": "Julie",
                    "lastName": "Fayet",
                    "email": "julie.fayet@outlook.com",
                    "password": "3465Tyue**",
                    "picture": "image1",
                    "address": {
                      "id": 1,
                      "town": null,
                      "postalCode": null,
                      "address": null
                    }
                }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Update user**
----
Update json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  ```json
        {
          "firstName": "Julie",
          "lastName": "Fayet",
          "email": "julie.fayet@outlook.com",
          "password": "3465Tyue**",
          "picture": "image1",
          "address": {
            "id": 1
          }
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
            {
              "id": 3,
              "firstName": "Julie",
              "lastName": "Fayet",
              "email": "julie.fayet@outlook.com",
              "password": "3465Tyue**",
              "picture": "image1",
              "address": {
                "id": 1,
                "town": null,
                "postalCode": null,
                "address": null
              }
            }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Delete user**
----
Delete json data about a single user.

* **URL**

      api/users/:id

* **Method:**

  `DELETE`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Update user/address**
----

Update json data about a single user.

* **URL**

      api/users/{idUser}/address/{idAdress}

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `None`

* **Data Params**

  `None`

    * **Success Response:**

        * **Code:** 200 OK


* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** { error : "Le user n'a pas été modifié" }

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/users/3/address/2",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```
----
# Garage
**Get all garage**
----
Returns array json data.

* **URL**

      api/garages


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK
              **Content:**
          ```json
                  [
                    {
                        "id": 1,
                        "name": "pro",
                         "telephone":"8976574544",
                         "email" : "pro@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "vehiculeLoan": false
                    },
                    {
                        "id": 2,
                        "name": "proGa",
                         "telephone":"8976545544",
                         "email" : "proGa@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "vehiculeLoan": true
                    }
                  ]
            ```

* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/garages",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```
---

**Get garage by id**
----
Returns json data about a single garage.

* **URL**

      api/garages/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200
          **Content:**
          ```json
                  {
                        "id": 1,
                        "name": "pro",
                         "telephone":"8976574544",
                         "email" : "pro@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "vehiculeLoan": false
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/garages/1",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Add garage**
----
Save a single garage in the data base.

* **URL**

      api/garages

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

  ```json
        {
          "id": 3,
          "name": "garageUni",
          "telephone":"8973274544",
          "email" : "gara@gmail.com",
          "address":{
            "id": 2
          },
          "vehiculeLoan": 1
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
                {
                  "id": 3,
                  "name": "garageUni",
                  "telephone":"8973274544",
                  "email" : "gara@gmail.com",
                  "address":{
                    "id": 2,
                    "town": null,
                    "postalCode": null,
                    "address": null
                  },
                  "vehiculeLoan": true
               }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/garages",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update garage**
----
Update json data about a single garage.

* **URL**

      api/garages/:id

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  ```json
        {
          "name": "garageUni",
          "telephone":"8973274544",
          "email" : "garage@gmail.com",
          "address":{
            "id": 2
          },
          "vehiculeLoan": 0
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
            {
                  "id": 3,
                  "name": "garageUni",
                  "telephone":"8973274544",
                  "email" : "garage@gmail.com",
                  "address":{
                    "id": 2,
                    "town": null,
                    "postalCode": null,
                    "address": null
                  },
                  "vehiculeLoan": false
               }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/garages/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Delete garage**
----
Delete json data about a single garage.

* **URL**

      api/garages/:id

* **Method:**

  `DELETE`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/garages/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Update garage/address**
----

Update json data about a single garage.

* **URL**

      api/garages/{idGarage}/address/{idAdress}

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `None`

* **Data Params**

  `None`

    * **Success Response:**

        * **Code:** 200 OK


* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** { error : "Le garage n'a pas été modifié" }

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/garages/3/address/2",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```

----
# Towing
**Get all towing**
----
Returns array json data.

* **URL**

      api/towings


* **url Params** : **Required**: None

    * **Data Params** : None

        * **Success Response:**

            * **Code:** 200 OK
              **Content:**
          ```json
                  [
                    {
                        "id": 1,
                        "name": "proTo",
                         "telephone":"8976574544",
                         "email" : "proTo@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "height": 1.5,
                         "width": 6.5,
                         "highway": true
                    },
                    {
                        "id": 2,
                        "name": "proGa",
                         "telephone":"8976545544",
                         "email" : "proGa@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "height": 2.5,
                         "width": 5.5,
                         "highway": false
                    }
                  ]
            ```

* **Sample Call:**

  ```javascript
    fetch.ajax({
      url: "api/towings",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```
---

**Get towing by id**
----
Returns json data about a single towing.

* **URL**

      api/towings/:id

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

    * **Success Response:**

        * **Code:** 200
          **Content:**
          ```json
                  {
                        "id": 2,
                        "name": "proGa",
                         "telephone":"8976545544",
                         "email" : "proGa@gmail.com",
                         "address":{
                            "id": 2,
                            "town": "PAU",
                            "postalCode": "64000",
                            "address": "34 rue Gaston Lacoste"
                          },
                         "height": 2.5,
                         "width": 5.5,
                         "highway": false
                    }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/towings/2",
      dataType: "json",
      type : "GET",
      success : function(response) {
        console.log(response);
      }
    });
  ```


**Add towing**
----
Save a single towing in the data base.

* **URL**

      api/towings

* **Method:**

  `POST`

*  **URL Params**

   None

* **Data Params**

  ```json
        {
          "name": "GAPro",
          "telephone":"0897456577",
          "email" : "GAPro@gmail.com",
          "address":{
            "id": 2
          },
          "height": 2.5,
          "width": 5.5,
          "highway": 0
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
                {
                  "id": 3,
                  "name": "GAPro",
                  "telephone":"0897456577",
                  "email" : "GAPro@gmail.com",
                  "address":{
                    "id": 2,
                    "town": null,
                    "postalCode": null,
                    "address": null
                  },
                  "height": 2.5,
                  "width": 5.5,
                  "highway": false
               }
          ```

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/towings",
      dataType: "json",
      type : "POST",
      success : function(response) {
        console.log(response);
      }
    });
  ```

**Update towing**
----
Update json data about a single towing.

* **URL**

      api/towings/:id

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  ```json
        {
          "name": "proGaT",
          "telephone":"8976545544",
          "email" : "proGa@gmail.com",
          "address":{
            "id": 2
          },
          "height": 2.5,
          "width": 5.5,
          "highway": 0
        }
  ```

    * **Success Response:**

        * **Code:** 201 CREATED
          **Content:**
          ```json
            {
              "name": "proGaT",
              "telephone":"8976545544",
              "email" : "proGa@gmail.com",
              "address":{
                "id": 2,
                "town": null,
                "postalCode": null,
                "address": null
              },
              "height": 2.5,
              "width": 5.5,
              "highway": false
          }
          ```

* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** `null`

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/towings/3",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Delete towing**
----
Delete json data about a single towing.

* **URL**

      api/towings/:id

* **Method:**

  `DELETE`

*  **URL Params**

   **Required:**

   `id=[Long]`

* **Data Params**

  None

* **Success Response:**

    * **Code:** 200 OK

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/towings/1",
      dataType: "json",
      type : "DELETE",
      success : function(response) {
        console.log(response);
      }
    });
  ```
**Update towing/address**
----

Update json data about a single towing.

* **URL**

      api/towings/{idTowings}/address/{idAdress}

* **Method:**

  ` PUT`

*  **URL Params**

   **Required:**

   `None`

* **Data Params**

  `None`

    * **Success Response:**

        * **Code:** 200 OK


* **Error Response:**

    * **Code:** 404 NOT FOUND
      **Content:** { error : "Le towing n'a pas été modifié" }

* **Sample Call:**

  ```javascript
    fetch({
      url: "api/towings/3/address/2",
      dataType: "json",
      type : "PUT",
      success : function(response) {
        console.log(response);
      }
    });
  ```


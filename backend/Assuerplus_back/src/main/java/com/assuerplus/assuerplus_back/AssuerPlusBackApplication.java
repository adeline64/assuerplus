package com.assuerplus.assuerplus_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.assuerplus.assuerplus_back.entity.Role;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.repository.RoleRepository;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

@SpringBootApplication
public class AssuerPlusBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssuerPlusBackApplication.class, args);
    }


/*
		@Bean
		CommandLineRunner commandLineRunner(UserRepository users, RoleRepository roles, PasswordEncoder encoder) {
			return args -> {
				Role roleUser = roles.save(new Role("USER"));
				Role roleAdmin = roles.save(new Role("ADMIN"));
				users.save(new User("pierre", "bordenave","user@toto.to", encoder.encode("user"), Set.of(new Role[]{roleUser})));
				users.save(new User("julie","patrick","admin@admin.to", encoder.encode("adminadmin"), Set.of(new Role[]{roleUser, roleAdmin})));
			};
		}


 */



}

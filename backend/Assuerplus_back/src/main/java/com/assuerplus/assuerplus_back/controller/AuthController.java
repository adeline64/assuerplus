package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.dto.LoginDTO;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.model.SecurityUser;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.service.JpaUserDetailsService;
import com.assuerplus.assuerplus_back.service.TokenService;
import com.assuerplus.assuerplus_back.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AuthController {

    private UserService userService;

    private JpaUserDetailsService userDetailsService;

    private final TokenService tokenService;

    private final AuthenticationManager authenticationManager;

    public AuthController(TokenService tokenService, AuthenticationManager authenticationManager, UserService userService, JpaUserDetailsService userDetailsService) {
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/token")
    public ResponseEntity<?> token(Principal principal) {
        if(principal != null){
            // Code à exécuter si principal n'est pas null
            String email = principal.getName();
            return ResponseEntity.ok(email);
        }
        else{
            // Code à exécuter si principal est null
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/api/users/login")
    public ResponseEntity<String> login(@RequestBody LoginDTO login, Principal principal) throws AuthenticationException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        User user = securityUser.getUser();
        String token = tokenService.generateToken(authentication, user.getId());
        return ResponseEntity.ok(token);
    }
}


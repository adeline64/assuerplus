package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.entity.Address;
import com.assuerplus.assuerplus_back.entity.Garage;
import com.assuerplus.assuerplus_back.service.AddressService;
import com.assuerplus.assuerplus_back.service.GarageService;
import com.assuerplus.assuerplus_back.service.TokenService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("/api/garages")
@RestController
public class GarageController {

    private GarageService garageService;

    private AddressService addressService;

    private TokenService tokenService;

    public GarageController(GarageService garageService, AddressService addressService, TokenService tokenService) {
        this.garageService = garageService;
        this.addressService = addressService;
        this.tokenService = tokenService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Garage> readGarage(){
        return garageService.readGarage();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public ResponseEntity<Garage> readOneGarage(@PathVariable Long id, Authentication authentication) {
        Optional<Garage> garage = garageService.readOneGarage(id);

        Long userId = getUserIdFromAuthentication(authentication);
        String token = tokenService.generateTokenWithGarageId(authentication, userId, id);

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);

        return garage.map(g -> ResponseEntity.ok().headers(headers).body(g))
                .map(response -> {
                    response.getHeaders().add("userId", userId.toString());
                    response.getHeaders().add("garageId", id.toString());
                    return response;
                })
                .orElse(ResponseEntity.notFound().build());
    }

    private Long getUserIdFromAuthentication(Authentication authentication) {
        return Long.valueOf(((Jwt) authentication.getPrincipal()).getClaims().get("userId").toString());
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PostMapping()
    public Garage createGarage(@RequestBody Garage garage){
        return garageService.createGarage(garage);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteGarage(@PathVariable Long id) {
        garageService.deleteGarage(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PutMapping("/{id}")
    public Garage updateGarage(@PathVariable Long id, @RequestBody Garage garage) {
        return garageService.updateGarage(id, garage);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PutMapping("/{idGarage}/address/{idAddress}")
    private Garage changeAddress(@PathVariable("idGarage") final Long idGarage, @PathVariable("idAddress") final Long idAddress) {
        Optional<Address> addressOptional = addressService.readOneAddress(idAddress);
        Optional <Garage>garageOptional = garageService.readOneGarage(idGarage);

        if (addressOptional.isPresent() && garageOptional.isPresent()){
            Address address = addressOptional.get();
            Garage garage = garageOptional.get();
            address.setConvenienceStoreList(address.getConvenienceStoreList());
            garage.setAddress(address);
            return garageService.updateGarage(idGarage, garage);
        } else {
            return null;
        }
    }

}


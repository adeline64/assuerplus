package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.entity.Picture;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.exception.InvalidLocationException;
import com.assuerplus.assuerplus_back.exception.UserNotFoundException;
import com.assuerplus.assuerplus_back.service.PictureService;
import com.assuerplus.assuerplus_back.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@RestController
@RequestMapping("/api/pictures")
public class PictureController {

    private PictureService pictureService;

    private final Path root = Paths.get("uploads");

    private UserService userService;

    public PictureController(PictureService pictureService, UserService userService) {
        this.pictureService = pictureService;
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping()
    public Picture createPicture(@RequestBody Picture picture){
        return pictureService.createPicture(picture);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping("/users/{idUser}/latitude/{lat}/longitude/{long}")
    public String uploadPicture(
            @PathVariable("idUser") final Long idUser,
            @RequestParam(value = "lat") Double latitude,
            @RequestParam(value = "long") Double longitude,
            @RequestParam("image")MultipartFile multipartFile) throws IOException
    {

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        if (!StringUtils.hasText(fileName)) {
            fileName = "default_filename.jpg";
        }

        Optional<User> userOptional = userService.readOneUser(idUser);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException("User with id " + idUser + " not found.");
        }
        User user = userOptional.get();

        if (latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180) {
            throw new InvalidLocationException("Invalid latitude/longitude values.");
        }

        // Create a directory for the user if it doesn't exist
        Path userDirectory = this.root.resolve(String.valueOf(idUser));
        if (!Files.exists(userDirectory)) {
            Files.createDirectory(userDirectory);
        }

        // Get the next available image number in the user's directory
        int nextImageNumber = 1;
        while (Files.exists(userDirectory.resolve(String.format("%d.jpg", nextImageNumber)))) {
            nextImageNumber++;
        }

        // Save the image to the user's directory with the next available image number
        Path imagePath = Paths.get(userDirectory.toString(), String.format("%d.jpg", nextImageNumber));
        Files.copy(multipartFile.getInputStream(), imagePath);

        // Compress the image
        compressImage(imagePath);

        try {

            String imageUrl = "uploads/" + idUser + "/" + nextImageNumber + ".jpg";

            Picture picture = new Picture();
            picture.setFileName(fileName);
            picture.setUserPicture(user);
            picture.setLatitude(latitude);
            picture.setLongitude(longitude);
            picture.setImageUrl(imageUrl);

            pictureService.createPicture(picture); // Save picture entity to database

            Files.copy(multipartFile.getInputStream(), this.root
                    .resolve(multipartFile.getOriginalFilename()));

            return "Uploaded the file successfully";

        } catch (Exception e) {

            throw new RuntimeException(e.getMessage());

        }

    }

    public static void compressImage(Path imagePath) throws IOException {
        BufferedImage originalImage = ImageIO.read(imagePath.toFile());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", outputStream);
        byte[] compressedImage = outputStream.toByteArray();
        Files.write(imagePath, compressedImage);
    }

}

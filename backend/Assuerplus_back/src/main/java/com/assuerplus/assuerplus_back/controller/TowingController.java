package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.entity.Address;
import com.assuerplus.assuerplus_back.entity.Towing;
import com.assuerplus.assuerplus_back.service.AddressService;
import com.assuerplus.assuerplus_back.service.TowingService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

@RequestMapping("/api/towings")
@RestController
public class TowingController {

    private TowingService towingService;

    private AddressService addressService;

    public TowingController(TowingService towingService, AddressService addressService) {
        this.towingService = towingService;
        this.addressService = addressService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Towing> readTowing(){
        return towingService.readTowing();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Towing> readOneTowing(@PathVariable Long id){
        return towingService.readOneTowing(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PostMapping()
    public Towing createTowing(@RequestBody Towing towing){
        return towingService.createTowing(towing);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteTowing(@PathVariable Long id) {
        towingService.deleteTowing(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PutMapping("/{id}")
    public Towing updateTowing(@PathVariable Long id, @RequestBody Towing towing) {
        return towingService.updateTowing(id, towing);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PutMapping("/{idTowing}/address/{idAddress}")
    private Towing changeAddress(@PathVariable("idTowing") final Long idTowing, @PathVariable("idAddress") final Long idAddress) {
        Optional<Address> addressOptional = addressService.readOneAddress(idAddress);
        Optional <Towing>towingOptional = towingService.readOneTowing(idTowing);

        if (addressOptional.isPresent() && towingOptional.isPresent()){
            Address address = addressOptional.get();
            Towing towing = towingOptional.get();
            address.setConvenienceStoreList(address.getConvenienceStoreList());
            towing.setAddress(address);
            return towingService.updateTowing(idTowing, towing);
        } else {
            return null;
        }
    }


}


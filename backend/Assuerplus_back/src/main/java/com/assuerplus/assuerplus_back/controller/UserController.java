package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.entity.Address;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.repository.PictureRepository;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.service.AddressService;
import com.assuerplus.assuerplus_back.service.PictureService;
import com.assuerplus.assuerplus_back.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    private AddressService addressService;

    private PictureService pictureService;

    private UserRepository userRepository;

    private PictureRepository pictureRepository;

    public UserController(UserService userService, AddressService addressService, PictureService pictureService, UserRepository userRepository, PictureRepository pictureRepository) {
        this.userService = userService;
        this.addressService = addressService;
        this.pictureService = pictureService;
        this.userRepository = userRepository;
        this.pictureRepository = pictureRepository;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<User> readUser() {
        return userService.readUser();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<User> readOneUser(@PathVariable Long id) {
        return userService.readOneUser(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PostMapping()
    public User createUser(@RequestBody User user) {
        System.out.println("passe");
        return userService.createUser(user);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PutMapping("/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User user) {
        return userService.updateUser(id, user);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PutMapping("/{idUser}/address/{idAddress}")
    private User changeAddress(@PathVariable("idUser") final Long idUser, @PathVariable("idAddress") final Long idAddress) {
        Optional<Address> addressOptional = addressService.readOneAddress(idAddress);
        Optional <User>userOptional = userService.readOneUser(idUser);

        if (addressOptional.isPresent() && userOptional.isPresent()){
            Address address = addressOptional.get();
            User user = userOptional.get();
            address.setUserList(address.getUserList());
            user.setAddress(address);
            return userService.updateUser(idUser, user);
        } else {
            return null;
        }
    }

}
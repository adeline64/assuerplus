package com.assuerplus.assuerplus_back.controller;

import com.assuerplus.assuerplus_back.entity.Garage;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.entity.Vehicule;
import com.assuerplus.assuerplus_back.service.GarageService;
import com.assuerplus.assuerplus_back.service.UserService;
import com.assuerplus.assuerplus_back.service.VehiculeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/api/vehicules")
public class VehiculeController {

    private UserService userService;

    private VehiculeService vehiculeService;

    private GarageService garageService;

    public VehiculeController(UserService userService, VehiculeService vehiculeService) {
        this.userService = userService;
        this.vehiculeService = vehiculeService;
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping()
    public Iterable<Vehicule> readVehicule() {
        return vehiculeService.readVehicule();
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @GetMapping("/{id}")
    public Optional<Vehicule> readOneVehicule(@PathVariable Long id) {
        return vehiculeService.readOneVehicule(id);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PostMapping()
    public Vehicule createVehicule(@RequestBody Vehicule vehicule) {
        return vehiculeService.createVehicule(vehicule);
    }

    @PreAuthorize("hasAuthority('SCOPE_USER')")
    @PostMapping("/{idVehicule}/users")
    public User addUsers(@PathVariable("idVehicule") Long id, @RequestBody User user) {
        return vehiculeService.addUsers(id, user);
    }

    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @PostMapping("/{idVehicule}/garages")
    public Garage addGarage(@PathVariable("idVehicule") Long id, @RequestBody Garage garage) {
        return vehiculeService.addGarage(id, garage);
    }

}


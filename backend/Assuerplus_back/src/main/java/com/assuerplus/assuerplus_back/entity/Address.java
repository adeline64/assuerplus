package com.assuerplus.assuerplus_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "town", nullable = false)
    private String town;

    @Column(name = "postal_code", nullable = false)
    private String postalCode;

    @Column(name = "address", nullable = false)
    private String address;

    @JsonIgnore
    @OneToMany(mappedBy = "address")
    private List<User> userList = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "address")
    private List<ConvenienceStore> convenienceStoreList = new ArrayList<>();


    public Address() {
    }

    public Address(Long id, String town, String postalCode, String address, List<User> userList, List<ConvenienceStore> convenienceStoreList) {
        this.id = id;
        this.town = town;
        this.postalCode = postalCode;
        this.address = address;
        this.userList = userList;
        this.convenienceStoreList = convenienceStoreList;
    }


}

package com.assuerplus.assuerplus_back.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "convenience_store")
public abstract class ConvenienceStore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Column(nullable = false, name = "email", unique = true)
    private String email;

    private String message;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @ManyToOne
    private Address address;
    public ConvenienceStore() {
    }

    public ConvenienceStore(Long id, String name, String telephone, String email, Address address) {
        this.id = id;
        this.name = name;
        this.telephone = telephone;
        this.email = email;
        this.address = address;
    }

}

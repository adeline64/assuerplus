package com.assuerplus.assuerplus_back.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "garage" )
public class Garage extends ConvenienceStore{

    @Column(name = "vehicle_loan", nullable = false)
    private Boolean vehiculeLoan;

    @JsonIgnore
    @ManyToMany(mappedBy = "garage")
    private Set<Vehicule> vehicules;


    public Garage() {
    }

    public Garage(Long id, String name, String telephone, String email, Address address, Boolean vehiculeLoan, Set<Vehicule> vehicules) {
        super(id, name, telephone, email, address);
        this.vehiculeLoan = vehiculeLoan;
        this.vehicules = vehicules;
    }
}

package com.assuerplus.assuerplus_back.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@Table(name = "towing" )
public class Towing extends ConvenienceStore{

    @Column(name = "height", nullable = false)
    private Float height;

    @Column(name = "width", nullable = false)
    private Float width;

    @Column(name = "highway", nullable = false)
    private Boolean highway;

    public Towing() {
    }

    public Towing(Long id, String name, String telephone, String email, Address address, Float height, Float width, Boolean highway) {
        super(id, name, telephone, email, address);
        this.height = height;
        this.width = width;
        this.highway = highway;
    }
}

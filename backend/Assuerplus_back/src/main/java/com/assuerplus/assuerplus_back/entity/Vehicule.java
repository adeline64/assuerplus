package com.assuerplus.assuerplus_back.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.time.Year;
import java.util.Set;

@Data
@Entity
@Table(name = "vehicule")
public class Vehicule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "brand", nullable = false)
    private String brand; //marque

    @Column(name = "model", nullable = false)
    private String model;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
    @Column(name = "year", nullable = false)
    private Year year;

    @Column(name = "color", nullable = false)
    private String color;

    @Column(name = "kilometer", nullable = false)
    private String kilometer;

    @Column(name = "fuel", nullable = false)
    private String fuel; //carburant

    @Column(name = "registration", nullable = false)
    private String registration; //immatriculation

    @ManyToMany
    @JoinTable(name = "user_vehicule",
            joinColumns = @JoinColumn(name = "vehicule_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> users;

    @ManyToMany
    @JoinTable(name = "garage_vehicule",
            joinColumns = @JoinColumn(name = "vehicule_id"),
            inverseJoinColumns = @JoinColumn(name = "garage_id"))
    private Set<Garage> garage;


    public Vehicule() {
    }


}

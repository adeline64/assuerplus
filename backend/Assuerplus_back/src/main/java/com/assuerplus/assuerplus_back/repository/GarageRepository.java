package com.assuerplus.assuerplus_back.repository;

import com.assuerplus.assuerplus_back.entity.Garage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GarageRepository extends JpaRepository<Garage, Long> {
}

package com.assuerplus.assuerplus_back.repository;

import com.assuerplus.assuerplus_back.entity.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PictureRepository extends JpaRepository<Picture,Long> {

}


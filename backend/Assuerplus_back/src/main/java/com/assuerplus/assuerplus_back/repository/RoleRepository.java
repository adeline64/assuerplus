package com.assuerplus.assuerplus_back.repository;


import com.assuerplus.assuerplus_back.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<Role, Long> {

    // crée méthode findByName pour récupérer les roles

    @Query("SELECT n FROM Role n WHERE n.name = :name")
    public Role findByName(String name);
}

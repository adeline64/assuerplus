package com.assuerplus.assuerplus_back.repository;

import com.assuerplus.assuerplus_back.entity.Towing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TowingRepository extends JpaRepository<Towing, Long> {
}

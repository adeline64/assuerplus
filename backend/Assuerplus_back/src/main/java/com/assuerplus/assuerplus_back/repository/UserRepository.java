package com.assuerplus.assuerplus_back.repository;

import com.assuerplus.assuerplus_back.entity.Picture;
import com.assuerplus.assuerplus_back.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User getUserByEmailAndPassword(String email, String password);

    Optional<User> findByEmail(String email);

    @Query("SELECT u FROM User u WHERE u.email = :email AND u.password = :password")
    public User findByEmailAndPassword(String email, String password);

    @Query("SELECT p FROM Picture p WHERE p.userPicture.id = :userId")
    List<Picture> findPicturesByUserId(Long userId);
}

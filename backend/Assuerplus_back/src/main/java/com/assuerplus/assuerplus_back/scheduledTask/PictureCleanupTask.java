package com.assuerplus.assuerplus_back.scheduledTask;

import com.assuerplus.assuerplus_back.service.PictureService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PictureCleanupTask {

    private static final int MAX_AGE_IN_DAYS = 7;

    private final Path root = Paths.get("uploads");

    private PictureService pictureService;

    public PictureCleanupTask(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @Scheduled(cron = "0 0 0 * * *") // Run every day at midnight
    public void cleanupPictures() throws IOException {
        Files.list(root)
                .filter(Files::isDirectory)
                .forEach(this::cleanupUserPictures);
    }

    private void cleanupUserPictures(Path userDirectory) {
        try {
            Instant now = Instant.now();
            Files.list(userDirectory)
                    .filter(Files::isRegularFile)
                    .filter(file -> {
                        try {
                            return Duration.between(Files.getLastModifiedTime(file).toInstant(), now).toSeconds() >= 89;
                        } catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    })
                    .forEach(file -> {
                        try {
                            Files.delete(file);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 0 0 * * *") // Executes every day at midnight
    public void cleanupOldPictures() throws IOException {
        System.out.println("Starting picture cleanup task...");

        // Get a list of all user directories
        try (DirectoryStream<Path> users = Files.newDirectoryStream(root)) {
            for (Path userDirectory : users) {

                // Iterate over all images in the user's directory
                try (DirectoryStream<Path> images = Files.newDirectoryStream(userDirectory)) {
                    for (Path imagePath : images) {

                        // Delete the image if it's older than the max age
                        FileTime fileTime = Files.getLastModifiedTime(imagePath);
                        LocalDateTime modifiedTime = fileTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                        if (modifiedTime.isBefore(LocalDateTime.now().minusDays(MAX_AGE_IN_DAYS))) {
                            Files.delete(imagePath);
                            System.out.println("Deleted old picture: " + imagePath);
                        }
                    }
                }

                // Delete the user directory if it's empty
                if (Files.list(userDirectory).count() == 0) {
                    Files.delete(userDirectory);
                    System.out.println("Deleted empty user directory: " + userDirectory);
                }
            }
        }
    }
}

package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.Address;

import java.util.Optional;

public interface AddressService {

    Iterable<Address> readAddress();

    Optional<Address> readOneAddress(Long id);

    Address createAddress(Address address);

    void deleteAddress(Long id);

    Address updateAddress(Long id, Address address);
}


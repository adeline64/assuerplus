package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.Garage;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface GarageService {
    Iterable<Garage> readGarage();

    Optional<Garage> readOneGarage(Long id);

    Garage createGarage(Garage garage);

    void deleteGarage(Long id);

    Garage updateGarage(Long id, Garage garage);

}


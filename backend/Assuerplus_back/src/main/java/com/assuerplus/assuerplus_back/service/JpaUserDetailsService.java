package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.model.SecurityUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public JpaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findByEmail(username)
                .map((User user) -> new SecurityUser(user))
                .orElseThrow(() -> new UsernameNotFoundException("not found" + username));
    }
}


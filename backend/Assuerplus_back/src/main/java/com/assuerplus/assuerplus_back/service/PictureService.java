package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.Picture;

public interface PictureService {

    Picture createPicture(Picture picture);
}

package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.Towing;

import java.math.BigDecimal;
import java.util.Optional;

public interface TowingService {

    Iterable<Towing> readTowing();

    Optional<Towing> readOneTowing(Long id);

    Towing createTowing(Towing towing);

    void deleteTowing(Long id);

    Towing updateTowing(Long id, Towing towing);
}


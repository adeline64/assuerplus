package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.User;

import java.util.Optional;

public interface UserService {
    Iterable<User> readUser();

    Optional<User> readOneUser(final Long id);

    User createUser(User user);

    void deleteUser(final Long id);

    User updateUser(final Long id, User user);

}


package com.assuerplus.assuerplus_back.service;

import com.assuerplus.assuerplus_back.entity.Garage;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.entity.Vehicule;

import java.util.Optional;

public interface VehiculeService {
    Iterable<Vehicule> readVehicule();

    Optional<Vehicule> readOneVehicule(Long id);

    Vehicule createVehicule(Vehicule vehicule);

    User addUsers(Long id, User user);

    Garage addGarage(Long id, Garage garage);
}

package com.assuerplus.assuerplus_back.service.implement;

import com.assuerplus.assuerplus_back.entity.Address;
import com.assuerplus.assuerplus_back.repository.AddressRepository;
import com.assuerplus.assuerplus_back.service.AddressService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Iterable<Address> readAddress() {
        return addressRepository.findAll();
    }

    @Override
    public Optional<Address> readOneAddress(final Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public Address createAddress(Address address) {
        return addressRepository.save(address);
    }

    @Override
    public void deleteAddress(final Long id) {
        addressRepository.deleteById(id);
    }

    @Override
    public Address updateAddress(final Long id, Address address) {
        var a = addressRepository.findById(id);
        if (a.isPresent()) {
            var courentAddress = a.get();
            courentAddress.setAddress(address.getAddress());
            courentAddress.setTown(address.getTown());
            courentAddress.setPostalCode(address.getPostalCode());
            courentAddress.setUserList(address.getUserList());
            courentAddress.setConvenienceStoreList(address.getConvenienceStoreList());
            return addressRepository.save(courentAddress);
        } else {
            return null;
        }
    }
}


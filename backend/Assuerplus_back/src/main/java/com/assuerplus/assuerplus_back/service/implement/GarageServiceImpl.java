package com.assuerplus.assuerplus_back.service.implement;

import com.assuerplus.assuerplus_back.entity.Garage;
import com.assuerplus.assuerplus_back.repository.GarageRepository;
import com.assuerplus.assuerplus_back.service.GarageService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GarageServiceImpl implements GarageService {

    private GarageRepository garageRepository;

    public GarageServiceImpl(GarageRepository garageRepository) {
        this.garageRepository = garageRepository;
    }

    @Override
    public Iterable<Garage> readGarage() {
        return garageRepository.findAll();
    }

    @Override
    public Optional<Garage> readOneGarage(final Long id) {
        return garageRepository.findById(id);
    }

    @Override
    public Garage createGarage(Garage garage) {
        return garageRepository.save(garage);
    }

    @Override
    public void deleteGarage(Long id) {
        garageRepository.deleteById(id);
    }

    @Override
    public Garage updateGarage(Long id, Garage garage) {
        var g = garageRepository.findById(id);
        if (g.isPresent()) {
            var courentGarage = g.get();
            courentGarage.setVehiculeLoan(garage.getVehiculeLoan());
            courentGarage.setName(garage.getName());
            courentGarage.setEmail(garage.getEmail());
            courentGarage.setTelephone(garage.getTelephone());
            courentGarage.setAddress(garage.getAddress());
            return garageRepository.save(courentGarage);
        } else {
            return null;
        }
    }
}


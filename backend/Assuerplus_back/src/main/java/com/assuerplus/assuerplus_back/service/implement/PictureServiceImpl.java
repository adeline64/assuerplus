package com.assuerplus.assuerplus_back.service.implement;

import com.assuerplus.assuerplus_back.entity.Picture;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.repository.PictureRepository;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.service.PictureService;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.util.stream.Stream;

@Service
public class PictureServiceImpl implements PictureService {

    private final Path root = Paths.get("uploads");

    private PictureRepository pictureRepository;

    private UserRepository userRepository;

    public PictureServiceImpl(PictureRepository pictureRepository, UserRepository userRepository) {
        this.pictureRepository = pictureRepository;
        this.userRepository = userRepository;
    }


    @Override
    public Picture createPicture(Picture picture) {
        return pictureRepository.save(picture);
    }
}


package com.assuerplus.assuerplus_back.service.implement;

import com.assuerplus.assuerplus_back.entity.Towing;
import com.assuerplus.assuerplus_back.repository.TowingRepository;
import com.assuerplus.assuerplus_back.service.TowingService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class TowingServiceImpl implements TowingService {

    private TowingRepository towingRepository;

    public TowingServiceImpl(TowingRepository towingRepository) {
        this.towingRepository = towingRepository;
    }

    @Override
    public Iterable<Towing> readTowing() {
        return towingRepository.findAll();
    }

    @Override
    public Optional<Towing> readOneTowing(Long id) {
        return towingRepository.findById(id);
    }

    @Override
    public Towing createTowing(Towing towing) {
        return towingRepository.save(towing);
    }

    @Override
    public void deleteTowing(Long id) {
        towingRepository.deleteById(id);
    }

    @Override
    public Towing updateTowing(Long id, Towing towing) {
        var t = towingRepository.findById(id);
        if (t.isPresent()) {
            var courentTowing = t.get();
            courentTowing.setName(towing.getName());
            courentTowing.setEmail(towing.getEmail());
            courentTowing.setTelephone(towing.getTelephone());
            courentTowing.setHeight(towing.getHeight());
            courentTowing.setWidth(towing.getWidth());
            courentTowing.setHighway(towing.getHighway());
            courentTowing.setAddress(towing.getAddress());
            return towingRepository.save(courentTowing);
        } else {
            return null;
        }
    }

}

package com.assuerplus.assuerplus_back.service.implement;


import com.assuerplus.assuerplus_back.entity.Address;
import com.assuerplus.assuerplus_back.entity.Role;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.repository.AddressRepository;
import com.assuerplus.assuerplus_back.repository.PictureRepository;
import com.assuerplus.assuerplus_back.repository.RoleRepository;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.slf4j.LoggerFactory;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {

    //private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final PictureRepository pictureRepository;

    private final RoleRepository roleRepository;

    private final AddressRepository addressRepository;

    private final AddressServiceImpl addressServiceImpl;


    public UserServiceImpl(UserRepository userRepository, PictureRepository pictureRepository, RoleRepository roleRepository, AddressRepository addressRepository, AddressServiceImpl addressServiceImpl) {
        this.userRepository = userRepository;
        this.pictureRepository = pictureRepository;
        this.roleRepository = roleRepository;
        this.addressRepository = addressRepository;
        this.addressServiceImpl = addressServiceImpl;
    }

    @Override
    public Iterable<User> readUser() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> readOneUser(final Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User createUser(User user) {

        Role userRole = roleRepository.findByName("USER");
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        user.setRoles(roles);

        if (user.getAddress() != null) {
            var address = new Address();
            address.setAddress(user.getAddress().getAddress());
            address.setTown(user.getAddress().getTown());
            address.setPostalCode(user.getAddress().getPostalCode());
            var addressSaved = addressRepository.save(address);
            user.setAddress(addressSaved);
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public void deleteUser(final Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User updateUser(final Long id, User user) {
        var u = userRepository.findById(id);
        if (u.isPresent()) {
            var courentUser = u.get();
            courentUser.setEmail(user.getEmail());
            courentUser.setPassword(user.getPassword());
            courentUser.setAddress(user.getAddress());
            courentUser.setLastName(user.getLastName());
            courentUser.setFirstName(user.getFirstName());
            courentUser.setPictureList(user.getPictureList());
            courentUser.setVehicules(user.getVehicules());
            courentUser.setRoles(user.getRoles());
            return userRepository.save(courentUser);
        } else {
            return null;
        }
    }
}


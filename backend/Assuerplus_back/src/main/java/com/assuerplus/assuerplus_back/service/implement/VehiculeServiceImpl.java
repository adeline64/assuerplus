package com.assuerplus.assuerplus_back.service.implement;

import com.assuerplus.assuerplus_back.entity.Garage;
import com.assuerplus.assuerplus_back.entity.User;
import com.assuerplus.assuerplus_back.entity.Vehicule;
import com.assuerplus.assuerplus_back.repository.GarageRepository;
import com.assuerplus.assuerplus_back.repository.UserRepository;
import com.assuerplus.assuerplus_back.repository.VehiculeRepository;
import com.assuerplus.assuerplus_back.service.VehiculeService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class VehiculeServiceImpl implements VehiculeService {

    private VehiculeRepository vehiculeRepository;

    private UserRepository userRepository;

    private GarageRepository garageRepository;

    public VehiculeServiceImpl(VehiculeRepository vehiculeRepository, UserRepository userRepository, GarageRepository garageRepository) {
        this.vehiculeRepository = vehiculeRepository;
        this.userRepository = userRepository;
        this.garageRepository = garageRepository;
    }

    @Override
    public Iterable<Vehicule> readVehicule() {
        return vehiculeRepository.findAll();
    }

    @Override
    public Optional<Vehicule> readOneVehicule(Long id) {
        return vehiculeRepository.findById(id);
    }

    @Override
    public Vehicule createVehicule(Vehicule vehicule) {
        return vehiculeRepository.save(vehicule);
    }

    @Override
    public User addUsers(Long id, User user) {
        Optional<User> u = userRepository.findById(id);
        if (u.isPresent()) {
            User currentUser = u.get();
            Set<Vehicule> vehicules = currentUser.getVehicules();
            vehicules.add(user.getVehicules().iterator().next());
            currentUser.setVehicules(vehicules);
            return userRepository.save(currentUser);
        } else {
            return null;
        }
    }

    @Override
    public Garage addGarage(Long id, Garage garage) {
        Optional<Garage> g = garageRepository.findById(id);
        if (g.isPresent()) {
            Garage currentGarage = g.get();
            Set<Vehicule> vehicules = currentGarage.getVehicules();
            vehicules.add(garage.getVehicules().iterator().next());
            currentGarage.setVehicules(vehicules);
            return garageRepository.save(currentGarage);
        } else {
            return null;
        }
    }
}


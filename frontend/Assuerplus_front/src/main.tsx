import React, {useState} from 'react';
import ReactDOM from 'react-dom/client';
import {
    createBrowserRouter,
    Link,
    Outlet,
    RouterProvider}
    from "react-router-dom";

import './index.css'
import './assets/css/Navbar.css'
import './assets/css/Home.css'
import "./assets/css/Login.css"
import "./assets/css/Declaration.css"

// Partie Context
import JsonContext from "./context/JsonContext";
import TokenContextProvider from "./context/TokenContextProvider";

//Page Files
import NavbarL from "./pages/NavbarL";
import Home from "./pages/Home";
import LoginForm from "./pages/LoginForm";
import DeclarationForm from "./pages/DeclarationForm";
import Logout from "./pages/Logout";

const HeaderLayout = () => (
    <>
        <header>
            <NavbarL />
        </header>
        <Outlet />
    </>
);

const router = createBrowserRouter([
{
    element: <HeaderLayout />,
    children: [
        {
            path: "/",
            element: <Home />,
        },
        {
            path: "/login",
            element: <LoginForm />
        },
        {
            path: "/contact",
           element: <DeclarationForm />
        },
        {
            path: "/logout",
            element: <Logout/>,
        }
    ]

}
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
      <TokenContextProvider>
        <RouterProvider router={router} />
      </TokenContextProvider>
  </React.StrictMode>,
)

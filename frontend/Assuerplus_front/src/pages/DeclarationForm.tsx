import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";
import JsonContext from "../context/JsonContext";
import { Form, Button, Alert } from "react-bootstrap";
import jwtDecode, {JwtPayload} from 'jwt-decode';

interface Picture {
    picture:File;
}

interface DecodedToken extends JwtPayload {
    userId: string;
}

interface Position {
    latitude: number;
    longitude: number;
}

export default function DeclarationForm() {

    const [pictures, setPictures] = useState<File | null>(null);
    const navigate = useNavigate();
    const [position, setPosition] = useState<Position | undefined>();
    const [errors, setErrors] = useState<string[]>([]);
    // @ts-ignore
    const {token, userId} = useContext(JsonContext);


    useEffect(() => {
        navigator.geolocation.getCurrentPosition(
            (pos) => {
                if (pos.coords) {
                    setPosition({
                        latitude: pos.coords.latitude,
                        longitude: pos.coords.longitude
                    });
                }
            },
            (error) => {
                console.error(error);
            }
        );
    }, []);

    function handleChange(event: React.ChangeEvent<HTMLInputElement>) {

        const inputElement = event.target as HTMLInputElement;

        const file = inputElement?.files?.[0];

        if (file) {
            setPictures(file);
        }
    }

    async function onDeclarationCreate(event: React.FormEvent) {
        event.preventDefault();

        const currentErrors: string[] = [];

        if (pictures === null) {
            currentErrors.push("Merci de mettre une photo");
        }

        if (currentErrors.length === 0) {

            try {

                if (position !== undefined) {

                    const {VITE_SERVER_ADDRESS} = import.meta.env;

                    const formData = new FormData();
                    if (pictures == null) {
                        return;
                    }

                    formData.append("image", pictures);

                    if (position !== undefined) {
                        formData.append("lat", position?.latitude.toString());
                        formData.append("long", position?.longitude.toString());
                    }

                    if (userId !== undefined) {
                        await axios.post(`${VITE_SERVER_ADDRESS}/api/pictures/users/${userId}/latitude/${position?.latitude ?? ''}/longitude/${position?.longitude ?? ''}`, formData, {
                            headers: {
                                'Content-Type': 'multipart/form-data',
                                Authorization: `Bearer ${token}`,
                            }
                        })
                            .then((response) => {
                                alert("l'image a bien été transmise et votre géolocalisation est : " + position?.latitude + " et " + position?.longitude )
                                if (navigate) {
                                    navigate("/");
                                }
                            })
                            .catch((error) => {
                                console.log(error)
                                setErrors([...errors, "Une erreur est survenue"]);
                            })
                    } else {
                        console.error("L'utilisateur n'est pas authentifié");
                    }

                } else {
                    console.error("La position est undefined.");
                }
            } catch (error) {
                console.error("Impossible d'envoyer la photo.", error);
            }
        } else {
            setErrors(currentErrors);
        }
    }

    return (
        <Form method="POST" onSubmit={onDeclarationCreate}>
            {errors.map((error) => (
                <Alert key={error} variant="danger">
                    {error}
                </Alert>
            ))}
            <Form.Group controlId="formPicture">
                <Form.Label>Picture:</Form.Label>
                <input type="file"
                       name="image"
                       accept="image/png, image/jpg, image/jpeg"
                       onChange={handleChange}/>
            </Form.Group>
            <Button variant="primary" type="submit">
                Envoyer
            </Button>
        </Form>
    );
};

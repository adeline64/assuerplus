import React from "react";

export default function Home() {

    return (

        <div id="home__presentation" className='home__presentation'>
            <h2>Bienvenue chez AssuerPlus</h2>

            <h4>
                Bienvenue sur notre site web dédié à l'assurance automobile !
            </h4>

            <p>
                Nous sommes une entreprise spécialisée dans les assurances pour les véhicules de tous types.
            </p>

            <p>
                Nous proposons une gamme complète de garanties adaptées à vos besoins et à votre budget.
            </p>

            <p>
                Que vous soyez un particulier ou une entreprise, nous avons la solution d'assurance qui vous convient.

            </p>


        </div>
    )
}

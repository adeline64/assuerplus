import React from 'react';
import { useNavigate } from 'react-router-dom';
import {Button} from "react-bootstrap";

export default function Logout() {
    const navigate = useNavigate();

    function handleSubmit() {
        localStorage.removeItem('TOKEN');
        navigate('/', { replace: true });
    }

    return (
        <div className="Logout" onClick={handleSubmit}>
            <Button variant="primary" type="submit">
                Se déconnecter
            </Button>
        </div>
    );
}

import React, {useContext} from "react";
import {Link, useNavigate} from "react-router-dom";
import {Nav, Navbar} from "react-bootstrap";
import JsonContext from "../context/JsonContext";

export default function NavbarL() {

    let navigate = useNavigate()

    // @ts-ignore
    const {token, setToken} = useContext(JsonContext);

    function logout() {
        localStorage.removeItem("TOKEN");
        setToken(null);
        navigate("/");
    }


    return (


        <Navbar className="nav" expand="lg" bg="dark" variant="dark">
            <Navbar.Brand/>
            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Link to="/" className="nav-link">
                        Accueil
                    </Link>
                    {token ? (
                        <>
                            <Link to="/contact" className="nav-link">
                                Déclaration
                            </Link>
                            <Link
                                to="/logout"
                                className="nav-link"
                                onClick={logout}
                            >
                                Déconnexion
                            </Link>
                        </>
                    ) : (
                        <Link to="/login" className="nav-link">
                            Connexion
                        </Link>
                    )}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
